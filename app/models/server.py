from app.extensions import db
from datetime import datetime

class Server(db.Model):
    __tablename__ = 'server'

    id = db.Column(db.Integer(), primary_key=True)
    url = db.Column(db.Integer(), unique=True, nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return "<Server: {}>".format(self.url)
