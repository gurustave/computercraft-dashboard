from app.extensions import db
from datetime import datetime

class Status(db.Model):
    __tablename__ = 'status'

    id = db.Column(db.Integer(), primary_key=True)

    description = db.Column(db.Text, nullable=False)
    timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    machine_id = db.Column(db.Integer, db.ForeignKey('machine.id'), nullable=False)
    machine = db.relationship('Machine', backref=db.backref('statuses', lazy=True))

    def __repr__(self):
        return "<Status: {} - {}>".format(self.machine.label, self.timestamp)
