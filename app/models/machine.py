from app.extensions import db

class Machine(db.Model):
    __tablename__ = 'machine'

    id = db.Column(db.Integer(), primary_key=True)
    label = db.Column(db.String(80), unique=True, nullable=False)

    server_id = db.Column(db.Integer, db.ForeignKey('server.id'), nullable=False)
    server = db.relationship('Server', backref=db.backref('machines', lazy=True))

    def __repr__(self):
        return "<Machine: {}>".format(self.label)
